# Scientific Computing Project #

The goal of the project was to port all the MPI codes written in C++ to Java via the use of Open MPI Java bindings.

All the original C++ MPI codes were provided in book:

Karniadakis and Kirby II. Parallel Scientific Computing in C++ and MPI. A Seamless Approach to Parallel Algorithms and their Implementation. Cambridge University Press 2003