import mpi.*;

public class Chapter2c11P {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;

		int rank = comm.getRank();
		int size = comm.getSize();

		int[] accum = {0};
		int[] sum = {0};
		int startval = 1000*rank/size+1;
		int endval = 1000*(rank+1)/size;

		for( int i = startval; i <= endval; i++){
			sum[0]+=i;
		}

		if( rank!=0 ){
			comm.send(sum, 1, MPI.INT, 0, 1);
		}else{
			for( int j = 1; j < size; j++){
				Status status = comm.recv(accum, 1, MPI.INT, j, 1);
				sum[0] += accum[0];
			}
		}

		if(rank == 0){
			System.out.println("The sum from 1 to 1000 is :" + sum[0]);
		}

		MPI.Finalize();
	}

}
