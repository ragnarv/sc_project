public class Chapter2c5 {

	public static void main(String[] args) {
		int initial_guess = 100;

		int xn = initial_guess;
		int i = 0;
		while (xn != 1) {
			System.out.println(i + " " + xn);

			if (xn % 2 == 0)
				xn = xn / 2;
			else
				xn = 3 * xn + 1;

			i = i + 1;
		}
	}
}
