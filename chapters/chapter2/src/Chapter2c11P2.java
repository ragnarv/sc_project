import mpi.*;
import java.nio.IntBuffer;

/**
Example on how to use direct buffers.
With blocking methods(e.g send) one can choose to use
either arrays or direct buffers. However, with non-blocking
methods(e.g iSend), only direct buffers can be used. 
*/
public class Chapter2c11P2 {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;

		int rank = comm.getRank();
		int size = comm.getSize();

		final int startval = 1000*rank/size+1;
		final int endval = 1000*(rank+1)/size;

		int sum = 0;
		for( int i = startval; i <= endval; i++){
			sum+=i;
		}

		final IntBuffer buf = MPI.newIntBuffer(1);
		if( rank!=0 ){
			buf.put(sum);
			comm.iSend(buf, 1, MPI.INT, 0, 1);
		}else{
			for( int j = 1; j < size; j++){
				final Request request = comm.iRecv(buf, 1, MPI.INT, j, 1);
				request.waitFor(); //block until operation is complete
				request.free(); //free object to prevent memory leak
				sum += buf.get();
			}
		}

		if(rank == 0){
			System.out.println("The sum from 1 to 1000 is :" + sum);
		}

		MPI.Finalize();
	}
}
