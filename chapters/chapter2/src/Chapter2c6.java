public class Chapter2c6 {

	public static void main(String[] args) {
		int initial_guess = 100;
		int max_iterations = 1000;

		int xn = initial_guess;
		for (int i = 0; i < max_iterations; i = i + 1) {
			System.out.println(i + " " + xn);
			if (xn == 1)
				break;
			if (xn % 2 == 0)
				xn = xn / 2;
			else
				xn = 3 * xn + 1;
		}
	}
}
