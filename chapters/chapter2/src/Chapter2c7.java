public class Chapter2c7 {
	
	public static void main(String[] args) {
		float fep = floatMachineEps();
		double dep = doubleMachineEps();
		
		System.out.println("Machine epsilon for single precision floating point numbers is: "+fep);
		System.out.println("Machine epsilon for double precision floating point numbers is: "+dep);
		
	}
	
	private static float floatMachineEps(){
		float fmachine_e = 1f;
		float ftest = 1f + fmachine_e;
		
		while(1f != ftest){
			fmachine_e = fmachine_e / 2f;
			ftest = 1f + fmachine_e;
		}
		return fmachine_e;
	}
	
	private static double doubleMachineEps(){
		double fmachine_e = 1d;
		double ftest = 1d + fmachine_e;
		
		while(1. != ftest){
			fmachine_e = fmachine_e / 2.;
			ftest = 1. + fmachine_e;
		}
		return fmachine_e;
	}
}

