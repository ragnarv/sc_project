import mpi.*;

public class Chapter2c9P {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		int rank = MPI.COMM_WORLD.getRank();
		int size = MPI.COMM_WORLD.getSize();

		System.out.println("Hello world from processor " + rank + " of " + size);

		MPI.Finalize();
	}

}
