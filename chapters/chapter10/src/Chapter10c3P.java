import mpi.*;
import java.util.Arrays;

public class Chapter10c3P {
  
  public static void main(String[] args) throws MPIException{
    MPI.Init(args);

    Comm comm = MPI.COMM_WORLD;

    int mynode = comm.getRank();
    int totalnodes = comm.getSize();

    final int size = 50;

    // Set up storage on each process
    double[] a = new double[size];
    double[] b = new double[size];
    double[] lambda = new double[size];
    double[][] Q =  new double[size][size];
    double[][] Q1 = new double[size][size];
    double[][] Q2 = new double[size][size];
    double[] xi = new double[size];
    double[] d  = new double[size];

    int[][] index = new int[totalnodes][totalnodes];
    double[][] adjust = new double[totalnodes][totalnodes];

    //Form the Matrix A
    for(int i=0;i<size;i++){
      a[i] = i+1.5;
      b[i] = 0.3;
    }

    //Set up recursive partitioning of the matrix.
    //Each process will solve for some subset of the problem

    index[0][0] = size; 
    for(int i=0;i<log2(totalnodes);i++){
      int isum = 0;
      for(int j=0;j<Math.pow(2,i);j++){
        index[i+1][2*j] = index[i][j]/2;
        index[i+1][2*j+1] = index[i][j] - index[i+1][2*j];
        isum += index[i+1][2*j];
        adjust[i][j] = b[isum-1];
        a[isum-1] = a[isum-1] - b[isum-1];
        a[isum]   = a[isum]   - b[isum-1];
        isum += index[i+1][2*j+1];
      }
    }


    // Each process solves recursively for its subpart of
    // the problem.

    int ioffset = log2(totalnodes);
    int isum = 0;
    for(int k=0;k<mynode;k++)
      isum += index[ioffset][k];
  
    TDQREigensolver(mynode,index[ioffset][mynode],a,isum,b,isum,d,0,Q1);

    // Fan-in algorithm to finish solving system

    int cnt,N1=0,N2=0;
    double bn = 0d;
    double[] in;
    for(int i=0;i<log2(totalnodes);i++){
      isum = 0; cnt = 0;
      for(int j=0;j<totalnodes;j+=(int)Math.pow(2,i)){
        if(mynode == j){
          if(isum%2==0){
            final int len = index[ioffset][isum+1];
            in = new double[len];
            comm.recv(in,len,MPI.DOUBLE,j+(int)Math.pow(2,i),1);
            fillPartOfArray(d,in,len,size);
            for(int k=0;k<index[ioffset][isum+1];k++){
              comm.recv(Q2[k],len,MPI.DOUBLE,j+(int)Math.pow(2,i),1);
            }   
            N1 = index[ioffset][isum];
            N2 = index[ioffset][isum+1];
            bn = adjust[ioffset-1][cnt++];
          }else{
            comm.send(d,index[ioffset][isum],MPI.DOUBLE,j-(int)Math.pow(2,i),1);
            for(int k=0;k<index[ioffset][isum];k++)
              comm.send(Q1[k],index[ioffset][isum],MPI.DOUBLE,j-(int)Math.pow(2,i),1);
          }
        }
        isum++;
      }
    
      for(int j=0;j<totalnodes;j+=(int)Math.pow(2,i+1)){
        if(mynode == j){
          cnt = 0;
          for(int k=0;k<N1;k++)
            xi[cnt++] = Q1[N1-1][k];
          for(int k=0;k<N2;k++)
            xi[cnt++] = Q2[0][k];

          // Solve for the secular equation to 
          // obtain eigenvalues
          SolveSecularEq(bn,N1+N2,d,xi,lambda,0);
    
          // Form the Q matrix from Q1 and Q2
          for(int k=0;k<N1;k++){
            for(int ll=0;ll<N1+N2;ll++){
              Q[k][ll] = 0.0;
              for(int m=0;m<N1;m++)
                Q[k][ll] += Q1[k][m]*xi[m]/(d[m]-lambda[ll]);
            }
          }

          for(int k=0;k<N2;k++){
            for(int ll=0;ll<N1+N2;ll++){
              Q[N1+k][ll] = 0.0;
              for(int m=0;m<N2;m++)
                Q[k+N1][ll] += Q2[k][m]*xi[N1+m]/(d[N1+m]-lambda[ll]);
            }
          }

          // Normalize the Q matrix so that each eigenvector
          // has length one
          double sum;
          for(int k=0;k<N1+N2;k++){
            sum = 0.0;
            for(int ll=0;ll<N1+N2;ll++)
              sum+= Q[ll][k]*Q[ll][k];
            sum = Math.sqrt(sum);
            for(int ll=0;ll<N1+N2;ll++)
              Q[ll][k] = Q[ll][k]/sum;
          }
  
          // Swap d and lambda arrays for use in the
          // next part of the fan-in algorithm
          double[] tmpd = d;
          d = lambda;
          lambda = tmpd;

          // Swap Q and Q1 for use in the
          // next part of the fan-in algorithm  
          double[][] tmpdd = Q1;
          Q1 = Q;
          Q = tmpdd;
        }          
      }
      ioffset = ioffset - 1;
    }


    if(mynode==0){
      System.out.println("The eigenvalues are: ");
      for(int k=0;k<size;k++)
        System.out.printf("%.4f\n",d[k]);
    }

    comm.barrier();

    MPI.Finalize();
  }

  private static int log2(int x){
    return (int) (Math.log(x)/Math.log(2));
  }

  private static void TDQREigensolver(int rank, int N, double[] a, int idxA, double[] b, int idxB, 
    double[] lambda, int idxL, double[][] Q){
    if(N == 1){
      Q[0][0] = 1.0;
      lambda[idxL] = a[idxA];
    }else{
      int N1 = N/2;
      int N2 = N-N1;

      double[] d = new double[N]; 
      double[][] Q1 = new double[N1][N1];
      double[][] Q2 = new double[N2][N2];
      
      a[idxA+N1-1] -= b[idxB+N1-1];
      a[idxA+N1] -= b[idxB+N1-1];

      TDQREigensolver(rank,N1,a,idxA,b,idxB,d,0,Q1);
      TDQREigensolver(rank,N2,a,idxA+N1,b,idxB+N1,d,N1,Q2);

      double[] xi = new double[N];
      int cnt = 0;
      for(int i=0;i<N1;i++)
        xi[cnt++] = Q1[N1-1][i];
      for(int i=0;i<N2;i++)
        xi[cnt++] = Q2[0][i];

      SolveSecularEq(b[idxB+N1-1],N,d,xi,lambda,idxL);

      for(int i=0;i<N1;i++){
        for(int j=0;j<N;j++){
          Q[i][j] = 0.0;
          for(int k=0;k<N1;k++)
            Q[i][j] += Q1[i][k]*xi[k]/(d[k]-lambda[idxL+j]);
        }
      }

      for(int i=0;i<N2;i++){
        for(int j=0;j<N;j++){
          Q[N1+i][j] = 0.0;
          for(int k=0;k<N2;k++)
            Q[i+N1][j] += Q2[i][k]*xi[N1+k]/(d[N1+k]-lambda[idxL+j]);
        }
      }

      double sum;
      for(int i=0;i<N;i++){
        sum = 0.0;
        for(int j=0;j<N;j++)
          sum+= Q[j][i]*Q[j][i];
        sum = Math.sqrt(sum);
        for(int j=0;j<N;j++)
          Q[j][i] = Q[j][i]/sum;
      }
    }
  }

  private static void SolveSecularEq(double bm, int N, double[] d, double[] xi, double[] lambda, int idxL){
    final double offset = 1.0e-5;
    final double tol = 1.0e-6;

    double xl,xr,xm;
    double yl,yr,ym;
    for(int i=0;i<N-1;i++){
      xl = d[i] + offset;
      yl = SecularEq(bm,N,d,xi,xl);
      xr = d[i+1] - offset;
      yr = SecularEq(bm,N,d,xi,xr);
      xm = 0.5*(xl+xr);
      ym = SecularEq(bm,N,d,xi,xm);
   
      if(yl*yr > 0){
        lambda[idxL+i] = xl;
        continue;
      }
      
      while(Math.abs(ym)>tol){
        if(yl*ym<0)
          xr = xm;
        else
          xl = xm;
        xm = 0.5*(xl+xr);
        ym = SecularEq(bm,N,d,xi,xm);
      }
      lambda[idxL+i] = xm;
    }

    xl = d[N-1] + offset;
    yl = SecularEq(bm,N,d,xi,xl);
    xr = 2*d[N-1];
    yr = SecularEq(bm,N,d,xi,xr);
    xm = 0.5*(xl+xr);
    ym = SecularEq(bm,N,d,xi,xm);
  
    if(yl*yr > 0){
      lambda[idxL+N-1] = xl;
    }else{
      while(Math.abs(ym)>tol){
        if(yl*ym<0)
          xr = xm;
        else
          xl = xm;
        xm = 0.5*(xl+xr);
        ym = SecularEq(bm,N,d,xi,xm);
      }
      lambda[idxL+N-1] = xm;
    }
  }

  private static void fillPartOfArray(double[] dstArray, double[] srcArray, int dstStart, int dstEnd){
    int i = 0;
    for(int j=dstStart;j<dstEnd;j++){
      dstArray[j] = srcArray[i++];
    }
  }

  private static double SecularEq(double bm, int N, double[] d, double[] xi, double x){
    double sum = 0.0;
    for(int i=0;i<N;i++){
      sum += xi[i]*xi[i]/(d[i]-x);
    }    
    return bm*sum + 1.0;
  }
}
