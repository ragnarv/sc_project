import mpi.*;

public class Chapter10c1P {
  
  public static void main(String[] args) throws MPIException{
    MPI.Init(args);

    Comm comm = MPI.COMM_WORLD;

    int mynode = comm.getRank();
    int totalnodes = comm.getSize();

    final int size = 10;
    final int maxit = 1000; //maximum number of iterations for power method
    final int numrows = size/totalnodes;

    double[][] A_local   = new double[numrows][size];
    double[][] A_local_t = new double[numrows][size];
    for(int i=0;i<numrows;i++){
      ChebyVandermonde(size,A_local[i],mynode*numrows + i);
    }
  
    /********************************************************/
    /* Use the power method to obtain the first eigenvector */
    /********************************************************/

    // Initialize starting vector to 1.0
    double[] xold = new double[size];
    for(int i=0;i<size;i++)
      xold[i] = 1d;  

    double[] xnew = new double[numrows];
    double[] tmparray = new double[size];
    for(int it=0;it<maxit;it++){
      // Matrix-vector multiplication
      for(int i=0;i<numrows;i++)
        xnew[i] = dot(size,A_local[i],xold);

      // Compute Euclidian norm of new vector
      double[] sum = new double[1];
      for(int i=0;i<numrows;i++)
        sum[0] += xnew[i]*xnew[i];
      
      double[] total = new double[1];
      comm.allReduce(sum,total,1,MPI.DOUBLE,MPI.SUM);
      total[0] = Math.sqrt(total[0]);

      // Scale vector by its norm
      for(int i=0;i<numrows;i++)
        xnew[i] = xnew[i]/total[0];
    
      // Gather (Allgather) new vector to all processors
      comm.allGather(xnew,numrows,MPI.DOUBLE,tmparray,numrows,MPI.DOUBLE);
    
      // Compute difference between old and new vector
      sum = new double[1];
      for(int i=0;i<size;i++){
        sum[0] += (xold[i]-tmparray[i])*(xold[i]-tmparray[i]);
        xold[i] = tmparray[i]; //replace old with new
      }
    
      if(Math.sqrt(sum[0]) < 1.0e-7) //termination condition
        break;
    }

    // Modify eigenvector per Householder transformation
    xold[0] = xold[0] + ((int)Math.signum(xold[0]));

    /**************************************************************/
    /* Compute S = A*H (S is a temporary state stored in A_local) */
    /**************************************************************/
  
    double tmps1 = dot(size,xold,xold);
    for(int i=0;i<numrows;i++){
      double tmps2 = dot(size,A_local[i],xold);
      for(int j=0;j<size;j++)
        A_local[i][j] = A_local[i][j] - 2.0*tmps2*xold[j]/tmps1;
    }

    /******************************/
    /* Tranpose temporary state S */
    /******************************/
    
    for(int i=0;i<numrows;i++){
      comm.allToAll(A_local[i],numrows,MPI.DOUBLE,tmparray,numrows,MPI.DOUBLE);
      int cnt = 0;
      for(int k=0;k<totalnodes;k++)
        for(int j=0;j<numrows;j++)
      A_local_t[j][i+k*numrows] = tmparray[cnt++];
    }

    /***************************/
    /* Compute G = H*S = H*A*H */
    /***************************/

    tmps1 = dot(size,xold,xold);
    for(int i=0;i<numrows;i++){
      double tmps2 = dot(size,A_local_t[i],xold);
      for(int j=0;j<size;j++)
        A_local_t[i][j] = A_local_t[i][j] - 2.0*tmps2*xold[j]/tmps1;
    }

    /********************************************************/
    /* Tranpose G so that it is stored by rows acrossed the */ 
    /* processors, just as the original A was stored        */
    /********************************************************/
  
    for(int i=0;i<numrows;i++){
      comm.allToAll(A_local_t[i],numrows,MPI.DOUBLE,tmparray,numrows,MPI.DOUBLE);
      int cnt = 0;
      for(int k=0;k<totalnodes;k++)
        for(int j=0;j<numrows;j++)
      A_local[j][i+k*numrows] = tmparray[cnt++];
    }
    
    if(mynode == 0){
      printMatrix(A_local,numrows,size);
    }
    
    comm.barrier();

    if(mynode == 1){
      printMatrix(A_local,numrows,size);
    }

    comm.barrier();

    MPI.Finalize();
  }

  private static void printMatrix(double[][] A, int numrows, int size){
    StringBuffer sb = new StringBuffer();
    for(int i=0;i<numrows;i++){
      for(int j=0;j<size;j++)
        sb.append(String.format("%.5f ",A[i][j]));
      sb.append("\n");
    }
    System.out.println(sb.toString());
  }

  private static double dot(int N, double[] a, double[] b){
    double sum = 0d;
  
    for(int i=0;i<N;i++)
      sum += a[i]*b[i];

    return sum;
  }

  private static void ChebyVandermonde(int npts, double[] A, int row){
    double[] x = ChebyshevPoints(npts);
  
    for(int j=0;j<npts;j++)
      A[j] = Math.pow(x[row],j);
  }

  private static double[] ChebyshevPoints(int npts){
    double[] x = new double[npts];
    double c1 = 4d*Math.atan(1d)/(npts*2d);

    for(int k=0;k<npts;k++)
      x[k] = Math.cos(c1*(2d*k+1d));

    return x;
  }

}
