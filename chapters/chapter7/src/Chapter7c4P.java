import mpi.*;
import static mpi.MPI.slice;

public class Chapter7c4P {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;

		int rank = comm.getRank();
		int size = comm.getSize();

    final int N = 20;
    double[][] A = new double[N][N];
    double[] x = new double[N];
    double[] q = new double[N];
    if(rank==0){
      for(int i=0;i<N;i++){
        q[i] = i+1;
        A[i][i] = -2.0;
        if(i<N-1){
          A[i][i+1] = 1.0;
          A[i+1][i] = 1.0;
        }
      }
    }

    Jacobi_P(comm,rank,size,N,A,x,q,1.0e-14);
  
    if(rank==0){
      for(int i=0;i<N;i++)
        System.out.println(x[i]);
    }

		MPI.Finalize();
	}

  private static int Jacobi_P(Comm comm, int mynode, int numnodes, int N, double[][] A, 
    double[] x, double[] b, double abstol) throws MPIException{

    final int maxit = 100000;
    int rows_local = (int) Math.floor(N/numnodes);
    int local_offset = mynode*rows_local;
    if(mynode == (numnodes-1)) 
      rows_local = N - rows_local*(numnodes-1);

    /*Distribute the Matrix and R.H.S. among the processors */
    if(mynode == 0){
      for(int i=1;i<numnodes-1;i++){
        for(int j=0;j<rows_local;j++)
          comm.send(A[i*rows_local+j],N,MPI.DOUBLE,i,j);
        comm.send(slice(b, i*rows_local),rows_local,MPI.DOUBLE,i,rows_local);
      }

      int last_rows_local = N-rows_local*(numnodes-1);
      for(int j=0;j<last_rows_local;j++)
          comm.send(A[(numnodes-1)*rows_local+j],N,MPI.DOUBLE,numnodes-1,j);
      comm.send(slice(b, (numnodes-1)*rows_local), last_rows_local,MPI.DOUBLE,numnodes-1,last_rows_local);
    }else{
      A = new double[rows_local][N];
      x = new double[rows_local];    
      b = new double[rows_local];
      for(int i=0;i<rows_local;i++)
        comm.recv(A[i],N,MPI.DOUBLE,0,i);
      comm.recv(b,rows_local,MPI.DOUBLE,0,rows_local);
    }

    double[] xold = new double[N];
    int[] count = new int[numnodes];
    int[] displacements = new int[numnodes];

    //set initial guess to all 1.0
    for(int i=0; i<N; i++){
      xold[i] = 1.0;
    }

    for(int i=0;i<numnodes;i++){
      count[i] = (int) Math.floor(N/numnodes);
      displacements[i] = i*count[i];
    }
    count[numnodes-1] = N - ((int)Math.floor(N/numnodes))*(numnodes-1);
  
    for(int k=0; k<maxit; k++){
      double error_sum_local = 0.0;
      for(int i = 0; i<rows_local; i++){
        int i_global = local_offset+i;
        double sum1 = 0.0; 
        double sum2 = 0.0;
        for(int j=0; j < i_global; j++)
          sum1 += A[i][j]*xold[j];
        for(int j=i_global+1; j < N; j++)
          sum2 += A[i][j]*xold[j];
      
        x[i] = (-sum1 - sum2 + b[i])/A[i][i_global];
        error_sum_local += (x[i]-xold[i_global])*(x[i]-xold[i_global]);
      } 

      double[] error_sum_global = new double[1];
      comm.allReduce(new double[]{error_sum_local},error_sum_global,1,MPI.DOUBLE,MPI.SUM);
      
      comm.allGatherv(x,rows_local,MPI.DOUBLE,xold,count,displacements,MPI.DOUBLE);
      
      if(Math.sqrt(error_sum_global[0])<abstol){
        if(mynode == 0){
          for(int i=0;i<N;i++)
              x[i] = xold[i];
        }
        return k;
      }
    }

    System.out.println("Jacobi: Maximum Number of Interations Reached Without Convergence");
    if(mynode == 0){
      for(int i=0;i<N;i++)
        x[i] = xold[i];
    }
    return maxit;
  }
}
