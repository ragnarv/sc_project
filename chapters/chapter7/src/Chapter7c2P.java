import mpi.*;

public class Chapter7c2P {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;

		int rank = comm.getRank();
		int size = comm.getSize();

  		final int npts = 40;
  		final double dx = 1.0/(npts-1);
  		final double dt = 0.25*dx*dx;

  		double[][] A = new double[npts][npts];
  		double[][] q = new double[npts][npts];

  		for(int i=0;i<npts;i++)
    		for(int j=0;j<npts;j++)
      			q[i][j] = -2.0*Math.PI*Math.PI*Math.sin(Math.PI*dx*i)*Math.sin(Math.PI*dx*j);
    
  		int itcount = Diffusion_Jacobi(npts,dx,dt,A,q,1.0e-14);
 
  		double sum = 0.0;
  		for(int i=0;i<npts;i++){
    		for(int j=0;j<npts;j++){
      			double truesoln = -Math.sin(Math.PI*dx*i)*Math.sin(Math.PI*dx*j);
      			sum += (A[i][j] - truesoln)*(A[i][j] - truesoln);
    		}
  		}

  		System.out.printf("Jacobi: L2 Error is %.10f in %d iterations\n", Math.sqrt(sum), itcount);

		MPI.Finalize();
	}

	private static int Diffusion_Jacobi(int N, double dx, double dt, double[][] A, double[][] q, double abstol){
  		final int maxit = 100000;
  		final double D = dt/(dx*dx);

  		double[][] Aold = new double[N][N];
  
  		for(int i=1; i<N-1; i++)
    		for(int j=1;j<N-1;j++)
      			Aold[i][j] = 1.0;
  
  		/* Boundary Conditions -- all zeros */
  		for(int i=0;i<N;i++){
    		A[0][i] = 0.0;
    		A[N-1][i] = 0.0;
    		A[i][0] = 0.0;
    		A[i][N-1] = 0.0;
  		}

  		for(int k=0; k<maxit; k++){
    		for(int i = 1; i<N-1; i++){
      			for(int j=1; j<N-1; j++){
					A[i][j] = dt*q[i][j] + Aold[i][j] +
	  					D*(Aold[i+1][j] + Aold[i][j+1] - 4.0*Aold[i][j] + 
	     				Aold[i-1][j] + Aold[i][j-1]);
      			}
    		}
    
    		double sum = 0.0;
    		for(int i=0;i<N;i++){
      			for(int j=0;j<N;j++){
					sum += (Aold[i][j]-A[i][j])*(Aold[i][j]-A[i][j]);
					Aold[i][j] = A[i][j];
      			}
    		}

    		if(Math.sqrt(sum)<abstol){
      			return k;
    		}
  		}
  
  		System.out.println("Jacobi: Maximum Number of Interations Reached Without Convergence");
  		return maxit;
	}
}
