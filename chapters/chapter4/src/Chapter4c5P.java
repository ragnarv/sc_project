import mpi.*;

public class Chapter4c5P {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;

		int rank = comm.getRank();
		int size = comm.getSize();

		final double globalA = -50.0;
  		final double globalB =  50.0;
  		final int levels = 10;

  		double localA = globalA + rank    *(globalB-globalA)/size;
  		double localB = globalA + (rank+1)*(globalB-globalA)/size;

  		double[] local_sum = {MidpointRule(levels, localA, localB)};
  		double[] answer = {0};
  		comm.reduce(local_sum,answer,1,MPI.DOUBLE,MPI.SUM,0);

	  	if(rank == 0){
	    	System.out.println("The value of the integral is: " + answer[0] );
	  	}

		MPI.Finalize();
	}

	private static double func(double x){
  		return(Math.sin(x)/x);
  	}

  	private static double MidpointRule(int level, double xleft, double xright){
	  	final int nsteps = (int) (Math.pow(2,level)-1);
	  	final double h = (xright-xleft)/Math.pow(2,level);

	  	double sum = 0.0;
	  	for(int i=0;i<=nsteps;i++)
	    	sum += func(xleft + (i+0.5)*h);
	  	return sum*h;
	}

}
