import mpi.*;
import java.util.Arrays;

public class Chapter6c1P {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;

		int rank = comm.getRank();
		int size = comm.getSize();

		final int N = 8;

		double[] a = new double[N];
		double[] b = new double[N];
		double[] c = new double[N];
		double[] q = new double[N];
		for(int i=0;i<N;i++){
    		a[i] = -2.0;
    		b[i] = c[i] = 1.0;
    		q[i] = (double)(i+1);
  		}

  		double[] x = ThomasAlgorithm_P(comm,rank,size,N,b,a,c,q);

  		if(rank==0){
  			for(int i=0;i<N;i++)
  				System.out.printf("%.4f\n",x[i]);
  		}

		MPI.Finalize();
	}

	private static double[] ThomasAlgorithm_P(Comm comm, int mynode, int numnodes, 
		       int N, double[] b, double[] a, double[] c, double[] q) throws MPIException{

	  	double[] l = new double[N];
	  	double[] d = new double[N];
	  	double[] y = new double[N];
	  
	  	for(int i=0;i<N;i++)
	    	l[i] = d[i] = y[i] = 0.0;

	    double[][] S = new double[2][2];
	  	S[0][0] = S[1][1] = 1.0;
	  	S[1][0] = S[0][1] = 0.0;

	  	int rows_local = (int) Math.floor(N/numnodes);
	  	int local_offset = mynode*rows_local;

	  	double s1tmp = 0d;
	  	double s2tmp = 0d;	  
	  	if(mynode==0){
	    	s1tmp = a[local_offset]*S[0][0];
	    	S[1][0] = S[0][0];
	    	S[1][1] = S[0][1];
	    	S[0][1] = a[local_offset]*S[0][1];
	    	S[0][0] = s1tmp;
	    	
	    	for(int i=1;i<rows_local;i++){
	      		s1tmp = a[i+local_offset]*S[0][0] - b[i+local_offset-1]*c[i+local_offset-1]*S[1][0]; 
	      		s2tmp = a[i+local_offset]*S[0][1] - b[i+local_offset-1]*c[i+local_offset-1]*S[1][1];
	      		S[1][0] = S[0][0];
	      		S[1][1] = S[0][1];
	      		S[0][0] = s1tmp;
	      		S[0][1] = s2tmp;
	    	}
	  	}else{
	    	for(int i=0;i<rows_local;i++){
	      		s1tmp = a[i+local_offset]*S[0][0] - b[i+local_offset-1]*c[i+local_offset-1]*S[1][0]; 
	      		s2tmp = a[i+local_offset]*S[0][1] - b[i+local_offset-1]*c[i+local_offset-1]*S[1][1];
	      		S[1][0] = S[0][0];
	      		S[1][1] = S[0][1];
	      		S[0][0] = s1tmp;
	      		S[0][1] = s2tmp;
	    	}
	  	}

	  	double[][] T = new double[2][2];
	  	for(int i=0; i<=log2(numnodes);i++){
	    	if(mynode+Math.pow(2,i) < numnodes){
	    		comm.send(flatten(S),4,MPI.DOUBLE,(int)(mynode+Math.pow(2,i)),0);
	    	}	
	    	if(mynode-Math.pow(2,i)>=0){
	    		double[] receivable = new double[4];
	    		comm.recv(receivable,4,MPI.DOUBLE,(int)(mynode-Math.pow(2,i)),0);
	      		T = unflatten(receivable,2);
	      		s1tmp = S[0][0]*T[0][0] + S[0][1]*T[1][0];
	      		S[0][1] = S[0][0]*T[0][1] + S[0][1]*T[1][1];
	      		S[0][0] = s1tmp;
	      		s1tmp = S[1][0]*T[0][0] + S[1][1]*T[1][0];
	      		S[1][1] = S[1][0]*T[0][1] + S[1][1]*T[1][1];
	      		S[1][0] = s1tmp;
	    	}
	  	}
	  
	  	d[local_offset+rows_local-1] = (S[0][0] + S[0][1])/(S[1][0] + S[1][1]);
	  	if(mynode == 0){
	  		comm.send(new double[]{d[local_offset+rows_local-1]},1,MPI.DOUBLE,1,0);
	  	}else{
	  		double[] receivable = new double[1];
	  		comm.recv(receivable,1,MPI.DOUBLE,mynode-1,0);
	  		d[local_offset-1] = receivable[0];
	    	if(mynode != numnodes-1){
	    		comm.send(new double[]{d[local_offset+rows_local-1]},1,MPI.DOUBLE,mynode+1,0);
	    	}
	  	}

	  	if(mynode == 0){
	    	l[0] = 0;
	    	d[0] = a[0];
	    	for(int i=1;i<rows_local-1;i++){
	      		l[local_offset+i] = b[local_offset+i-1]/d[local_offset+i-1];
	      		d[local_offset+i] = a[local_offset+i] - l[local_offset+i]*c[local_offset+i-1];
	    	}
	    	l[local_offset+rows_local-1] = b[local_offset+rows_local-2]/d[local_offset+rows_local-2];
	  	}else{
	    	for(int i=0;i<rows_local-1;i++){
	      		l[local_offset+i] = b[local_offset+i-1]/d[local_offset+i-1];
	      		d[local_offset+i] = a[local_offset+i] - l[local_offset+i]*c[local_offset+i-1];
	    	}
	    	l[local_offset+rows_local-1] = b[local_offset+rows_local-2]/d[local_offset+rows_local-2];
	  	}

	  /***********************************************************************************/

	  	if(mynode>0)
	    	d[local_offset-1] = 0;
	  
	  	double[] tmp = Arrays.copyOf(d, d.length);
	    comm.allReduce(tmp,d,N,MPI.DOUBLE,MPI.SUM);
	  	
	  	tmp = Arrays.copyOf(l, l.length);
	    comm.allReduce(tmp,l,N,MPI.DOUBLE,MPI.SUM);

	  	double[] x = new double[N];
	  	if(mynode ==0){    
	    	/* Forward Substitution [L][y] = [q] */
	    	y[0] = q[0];
	    	for(int i=1;i<N;i++){
	    		y[i] = q[i] - l[i]*y[i-1];
	    	}
	    
	    	/* Backward Substitution [U][x] = [y] */
	    	x[N-1] = y[N-1]/d[N-1];
	    	for(int i=N-2;i>=0;i--){
	    		x[i] = (y[i] - c[i]*x[i+1])/d[i];
	    	}	
	  	}
	  	return x;
	}

	private static int log2(int x){
		return (int)(Math.log(x)/Math.log(2));
	}

	private static double[] flatten(double[][] unpacked){
		double[] packed = new double[unpacked.length*unpacked[0].length];
		int k = 0;
		for(int i=0;i<unpacked.length;i++){
			for(int j=0;j<unpacked[i].length;j++){
				packed[k++] = unpacked[i][j];
			}
		}
		return packed;
	}

	private static double[][] unflatten(double[] packed, int size){
		final int arrCount = (int)(packed.length/size);
		double[][] unpacked = new double[arrCount][size];
		for(int i=0;i<arrCount;i++){
			unpacked[i] = Arrays.copyOfRange(packed, i*size, i*size+size);
		}
		return unpacked;
	}
}
