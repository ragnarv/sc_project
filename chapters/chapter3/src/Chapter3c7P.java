import mpi.*;

public class Chapter3c7P {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;

		int rank = comm.getRank();
		int size = comm.getSize();

		final int nitems = 10;
		double[] array = new double[nitems];
	  	
	  	if(rank == 0){
	    	for(int i=0;i<nitems;i++)
	      		array[i] = i;
  		}

	  	if(rank==0){
	  		for(int i=1;i<size;i++)
	  			comm.send(array, nitems, MPI.DOUBLE, i, 1);
	  	}else{
	  		Status status = comm.recv(array, nitems, MPI.DOUBLE, 0, 1);
	  	}

	  	for(int i=0;i<nitems;i++){
	  		System.out.println("Processor " + rank + ": array[" + i + "] = " + array[i]);
	  	}

		MPI.Finalize();
	}

}
