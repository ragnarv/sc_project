import mpi.*;

public class Chapter5c4P {

  private static enum MPIType{
    MPI_SEND_RECV, MPI_SEND_RECV_REPLACE;
  }
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;
    final MPIType mpiType = MPIType.MPI_SEND_RECV_REPLACE;

		int rank = comm.getRank();
		int size = comm.getSize();

		final int levels = 10;
		final double global_a = 0.0;
		final double global_b = 1.0;

  	if(rank == 0)
    	System.out.println("npts\tError (First Deriv)\tError (Second Deriv)");

  	for(int i=3;i<levels+2;i++){
    	int global_npts = (int) Math.pow(2,i);    
    	int local_npts  = global_npts/size;
    	global_npts = local_npts*size;
    	double dx = (global_b-global_a)/(global_npts-1);

    	double local_a = global_a + dx*local_npts*rank;

    	double[] u = new double[local_npts];
    	for(int j=0;j<local_npts;j++)
      		u[j] = func(local_a + j*dx);
    	
    	double[] u_x = new double[local_npts];
    	SO_FirstDeriv_1DperP(comm,local_npts,dx,u,u_x,rank,size,mpiType);

    	double[] u_xx = new double[local_npts];
    	SO_SecondDeriv_1DperP(comm,local_npts,dx,u,u_xx,rank,size,mpiType);

    	double[] ux_error = {0.0};
    	double[] uxx_error = {0.0};
    	for(int j=0;j<local_npts;j++){
      		ux_error[0]  += dx*Math.pow((u_x[j]-funcFirstDer(local_a + j*dx)),2);
      		uxx_error[0] += dx*Math.pow((u_xx[j]-funcSecondDer(local_a + j*dx)),2);
    	}
    
    	double[] answer1 = {0};
    	double[] answer2 = {0};
    	comm.reduce(ux_error,answer1,1,MPI.DOUBLE,MPI.SUM,0);
    	comm.reduce(uxx_error,answer2,1,MPI.DOUBLE,MPI.SUM,0);
    
     	if(rank==0){
     		System.out.printf("%d \t %.10f \t %.10f\n", global_npts, Math.sqrt(answer1[0]), Math.sqrt(answer2[0]));
    	}
    }

		MPI.Finalize();
	}

	/* Second Order First Derivative in 1D, periodic boundaries -- Parallel */
	private static void SO_FirstDeriv_1DperP (Comm comm, int npts, double dx, double[] u, double[] u_x, 
			   int mynode, int totalnodes, MPIType mpiType) throws MPIException{
		
    final double two_invdx = 1.0/(2.0*dx);
		double[] mpitemp = new double[1];

		for(int i=1;i<npts-1;i++)
  		u_x[i] = (u[i+1]-u[i-1])*two_invdx;

		if(mynode == 0){
      if(mpiType == MPIType.MPI_SEND_RECV){
    		comm.send(new double[]{u[npts-1]},1,MPI.DOUBLE,1,1);
    		comm.recv(mpitemp,1,MPI.DOUBLE,1,1); 
    		u_x[npts-1] = (mpitemp[0] - u[npts-2])*two_invdx;  

    		comm.send(new double[]{u[0]},1,MPI.DOUBLE,totalnodes-1,1);
    		comm.recv(mpitemp,1,MPI.DOUBLE,totalnodes-1,1);     
    		u_x[0] = (u[1]-mpitemp[0])*two_invdx;
      }else{
        mpitemp = new double[]{u[npts-1]};
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,1,1,1,1);
        u_x[npts-1] = (mpitemp[0] - u[npts-2])*two_invdx;  

        mpitemp[0] = u[0];
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,totalnodes-1,1,totalnodes-1,1);
        u_x[0] = (u[1]-mpitemp[0])*two_invdx;  
      }
		}else if(mynode == (totalnodes-1)){
      if(mpiType == MPIType.MPI_SEND_RECV){
    		comm.recv(mpitemp,1,MPI.DOUBLE,mynode-1,1); 
    		u_x[0] = (u[1]-mpitemp[0])*two_invdx;

    		comm.send(new double[]{u[0]},1,MPI.DOUBLE,mynode-1,1);

    		comm.recv(mpitemp,1,MPI.DOUBLE,0,1); 
    		u_x[npts-1] = (mpitemp[0]-u[npts-2])*two_invdx;
    
    		comm.send(new double[]{u[npts-1]},1,MPI.DOUBLE,0,1);
      }else{
        mpitemp = new double[]{u[0]};
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,mynode-1,1,mynode-1,1);
        u_x[0] = (u[1]-mpitemp[0])*two_invdx;

        mpitemp[0] = u[npts-1];
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,0,1,0,1);
        u_x[npts-1] = (mpitemp[0] - u[npts-2])*two_invdx;
      }
		}else{
      if(mpiType == MPIType.MPI_SEND_RECV){
    		comm.recv(mpitemp,1,MPI.DOUBLE,mynode-1,1); 
    		u_x[0] = (u[1]-mpitemp[0])*two_invdx;

    		comm.send(new double[]{u[0]},1,MPI.DOUBLE,mynode-1,1);

    		comm.send(new double[]{u[npts-1]},1,MPI.DOUBLE,mynode+1,1);
    		comm.recv(mpitemp,1,MPI.DOUBLE,mynode+1,1);     
    		u_x[npts-1] = (mpitemp[0]-u[npts-2])*two_invdx;
      }else{
        mpitemp = new double[]{u[0]};
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,mynode-1,1,mynode-1,1);
        u_x[0] = (u[1]-mpitemp[0])*two_invdx;
  
        mpitemp[0] = u[npts-1];
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,mynode+1,1,mynode+1,1);
        u_x[npts-1] = (mpitemp[0]-u[npts-2])*two_invdx;
      }
		}
	}

	/* Second Order Second Derivative in 1D, periodic boundaries -- Parallel */
	private static void SO_SecondDeriv_1DperP (Comm comm, int npts, double dx, double[] u, double[] u_xx, 
			    int mynode, int totalnodes, MPIType mpiType) throws MPIException{

		final double inv_dx2 = 1.0/(dx*dx);
		double[] mpitemp = new double[1];

		for(int i=1;i<npts-1;i++)
  		u_xx[i] = (u[i+1]-2.0*u[i]+u[i-1])*inv_dx2;

		if(mynode == 0){
      if(mpiType == MPIType.MPI_SEND_RECV){	
    		comm.send(new double[]{u[npts-1]},1,MPI.DOUBLE,1,1);

    		comm.recv(mpitemp,1,MPI.DOUBLE,1,1); 
    		u_xx[npts-1] = (mpitemp[0] - 2.0*u[npts-1] + u[npts-2])*inv_dx2;  

    		comm.send(new double[]{u[0]},1,MPI.DOUBLE,totalnodes-1,1);
    		comm.recv(mpitemp,1,MPI.DOUBLE,totalnodes-1,1);     
    		u_xx[0] = (u[1]-2.0*u[0]+mpitemp[0])*inv_dx2;
      }else{
        mpitemp = new double[]{u[npts-1]};
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,1,1,1,1);
        u_xx[npts-1] = (mpitemp[0] - 2.0*u[npts-1] + u[npts-2])*inv_dx2;  

        mpitemp[0] = u[0];
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,totalnodes-1,1,totalnodes-1,1);
        u_xx[0] = (u[1] - 2.0*u[0] + mpitemp[0])*inv_dx2;  
      }
		}else if(mynode == (totalnodes-1)){
      if(mpiType == MPIType.MPI_SEND_RECV){
    		comm.recv(mpitemp,1,MPI.DOUBLE,mynode-1,1); 
    		u_xx[0] = (u[1] - 2.0*u[0] + mpitemp[0])*inv_dx2;

    		comm.send(new double[]{u[0]},1,MPI.DOUBLE,mynode-1,1);

    		comm.recv(mpitemp,1,MPI.DOUBLE,0,1); 
    		u_xx[npts-1] = (mpitemp[0]-2.0*u[npts-1]+u[npts-2])*inv_dx2;
    
    		comm.send(new double[]{u[npts-1]},1,MPI.DOUBLE,0,1);
      }else{
        mpitemp = new double[]{u[0]};
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,mynode-1,1,mynode-1,1);
        u_xx[0] = (u[1] - 2.0*u[0] + mpitemp[0])*inv_dx2;

        mpitemp[0] = u[npts-1];
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,0,1,0,1);
        u_xx[npts-1] = (mpitemp[0] -  2.0*u[npts-1] + u[npts-2])*inv_dx2;
      }
		}else{
      if(mpiType == MPIType.MPI_SEND_RECV){
    		comm.recv(mpitemp,1,MPI.DOUBLE,mynode-1,1); 
    		u_xx[0] = (u[1] -2.0*u[0] + mpitemp[0])*inv_dx2;
    		comm.send(new double[]{u[0]},1,MPI.DOUBLE,mynode-1,1);

    		comm.send(new double[]{u[npts-1]},1,MPI.DOUBLE,mynode+1,1);
    		comm.recv(mpitemp,1,MPI.DOUBLE,mynode+1,1);     
    		u_xx[npts-1] = (mpitemp[0] -2.0*u[npts-1] + u[npts-2])*inv_dx2;
      }else{
        mpitemp = new double[]{u[0]};
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,mynode-1,1,mynode-1,1);
        u_xx[0] = (u[1] -2.0*u[0] + mpitemp[0])*inv_dx2;
    
        mpitemp[0] = u[npts-1];
        comm.sendRecvReplace(mpitemp,1,MPI.DOUBLE,mynode+1,1,mynode+1,1);
        u_xx[npts-1] = (mpitemp[0] -2.0*u[npts-1] + u[npts-2])*inv_dx2;
      }
		}
	}

	private static double func(double x){
		return(Math.sin(2.0*Math.PI*x));
	}	

	private static double funcFirstDer(double x){
		return(2.0*Math.PI*Math.cos(2.0*Math.PI*x));
	}

	private static double funcSecondDer(double x){
		return(-4.0*Math.PI*Math.PI*Math.sin(2.0*Math.PI*x));
	}
}
