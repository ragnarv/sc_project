import mpi.*;

public class Chapter9c0P {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;

		int rank = comm.getRank();
		int totalnodes = comm.getSize();

		final int size = 10;
		final int numrows = size/totalnodes;
  		
    double[][] A_local = new double[numrows][size+1];
  	int[] myrows = new int[numrows];

  	/* PART 2 */
  	double[] xpts = ChebyshevPoints(size);
  
  	for(int i=0;i<numrows;i++){
    	int index = rank + totalnodes*i;
    	myrows[i] = index;
    	ChebyVandermonde(size,A_local[i],index);

    	// Set-up right-hand-side as the Runge function
    	A_local[i][size] = 1.0/(1.0+25.0*xpts[index]*xpts[index]);
  	}

  	double[] tmp = new double[size+1];
 		double[] x = new double[size];

  	/* PART 3 */
  	/* Gaussian Elimination of the augmented matrix */
  	int cnt = 0;

  	for(int i=0;i<size-1;i++){
    	if(cnt < myrows.length && i == myrows[cnt]){
  			comm.bcast(A_local[cnt],size+1,MPI.DOUBLE,rank);//With bcast root process sends, others recieve
   			for(int j=0;j<size+1;j++)
					 tmp[j] = A_local[cnt][j];
      	cnt++;
    	}else{
      	comm.bcast(tmp,size+1,MPI.DOUBLE,i%totalnodes);
    	}

    	for(int j=cnt;j<numrows;j++){
      	double scaling = A_local[j][i]/tmp[i];
      	for(int k=i;k<size+1;k++)
					A_local[j][k] = A_local[j][k] - scaling*tmp[k]; 
    	}
  	}

  	/* PART 4 */

  	/* On each processor, initialize the value of x as equal to
     	the modified (by Gaussian elimination) right-hand-side if
     	that information is on the processor, otherwise initialize
     	to zero. 
  	*/

  	cnt = 0;
  	for(int i=0;i<size;i++){
    	if(cnt < myrows.length && i == myrows[cnt]){
      	x[i] = A_local[cnt][size];
      	cnt++;
      }else{
    	 x[i] = 0;
      }
  	}

  	/* PART 5 */
  	/* Backsolve to find the solution x */
  	cnt = numrows-1;
    double[] in = new double[1];
  	for(int i=size-1;i>0;i--){
    	if(cnt>=0){
      	if(i == myrows[cnt]){
					x[i] = x[i]/A_local[cnt][i];
					comm.bcast(new double[]{x[i]},1,MPI.DOUBLE,rank);
					cnt--;
      	}else{
      		comm.bcast(in,1,MPI.DOUBLE,i%totalnodes);
          x[i]=in[0];
      	}
    	}else{
    		comm.bcast(in,1,MPI.DOUBLE,i%totalnodes);
        x[i] = in[0];
    	}

    	for(int j=0;j<=cnt;j++)
      	x[myrows[j]] = x[myrows[j]] - A_local[j][i]*x[i];
  	}
  
  	if(rank==0){
    	x[0] = x[0]/A_local[cnt][0];
    	comm.bcast(new double[]{x[0]},1,MPI.DOUBLE,0);
  	}else{
  		comm.bcast(in,1,MPI.DOUBLE,0);
      x[0]=in[0];
  	}
  
  	/* PART 6 */

  	if(rank==0){
    	for(int i=0;i<size;i++)
      	System.out.println(x[i]);
  	}

		MPI.Finalize();
	}

	private static void ChebyVandermonde(int npts, double[] A, int row){
  		double[] x = ChebyshevPoints(npts);
  
  		for(int j=0;j<npts;j++)
    		A[j] = Math.pow(x[row],j);
	}

	private static double[] ChebyshevPoints(int npts){
  		double[] x = new double[npts];
  		double c1 = 4.0*Math.atan(1.0)/(npts*2.0);

  		for(int k=0;k<npts;k++)
    		x[k] = Math.cos(c1*(2.0*k+1.0));

  		return x;
	}
}
