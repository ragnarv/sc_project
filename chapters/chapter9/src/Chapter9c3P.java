import mpi.*;
import java.util.Arrays;

public class Chapter9c3P {
  
  public static void main(String[] args) throws MPIException{
    MPI.Init(args);

    Comm comm = MPI.COMM_WORLD;

    int mynode = comm.getRank();
    int totalnodes = comm.getSize();

    final int rows_per_proc = 40;
    final double c1 = 20.0;
    final double c2 = 1000.0;
    final double tol = 1.0e-14;

    final int totalsize = totalnodes*rows_per_proc;
    final double dx = 1.0/(totalsize+1);

    double[] p = new double[totalsize];
    double[] z = new double[rows_per_proc];
    double[] r = new double[rows_per_proc];
    double[] mr = new double[rows_per_proc];

    double[] x = new double[rows_per_proc];
    double[] q = new double[rows_per_proc];
    double[] grid = new double[rows_per_proc];
    
    for(int i=0;i<rows_per_proc;i++){
      grid[i] = dx*(1+rows_per_proc*mynode+i);
      q[i] = -dx*dx*Math.sin(2.0*Math.PI*grid[i])*
        (-4.0*Math.PI*Math.PI - c2*Math.exp(c1*(grid[i]-0.5)*(grid[i]-0.5)));
      x[i] = 1.0;
    }

    /* Part 2 */

    double[][] A = new double[rows_per_proc][totalsize];

    if(mynode==0){
      A[0][0] = 2.0 + dx*dx*c2*Math.exp(c1*(grid[0]-0.5)*(grid[0]-0.5)); 
      A[0][1] = -1.0;
      for(int i=1;i<rows_per_proc;i++){
        A[i][i] = 2.0 + dx*dx*c2*Math.exp(c1*(grid[i]-0.5)*(grid[i]-0.5)); 
        A[i][i-1] = -1.0;
        A[i][i+1] = -1.0;
      }
    }else if(mynode == (totalnodes-1)){
      A[rows_per_proc-1][totalsize-1] = 2.0 + 
          dx*dx*c2*Math.exp(c1*(grid[rows_per_proc-1]-0.5)*(grid[rows_per_proc-1]-0.5));  
      A[rows_per_proc-1][totalsize-2] = -1.0;
      for(int i=0;i<rows_per_proc-1;i++){
        int offset = rows_per_proc*mynode + i;
        A[i][offset] = 2.0 + dx*dx*c2*Math.exp(c1*(grid[i]-0.5)*(grid[i]-0.5)); 
        A[i][offset-1] = -1.0;
        A[i][offset+1] = -1.0;
      }
    }else{
      for(int i=0;i<rows_per_proc;i++){
        int offset = rows_per_proc*mynode + i;
        A[i][offset] = 2.0 + dx*dx*c2*Math.exp(c1*(grid[i]-0.5)*(grid[i]-0.5)); 
        A[i][offset-1] = -1.0;
        A[i][offset+1] = -1.0;
      }
    }

    /* Part 3 */

    int offset = mynode*rows_per_proc;
 
    for(int i=0;i<totalsize;i++)
      p[i] = 1.0;

    for(int i=0;i<rows_per_proc;i++){
      r[i] = q[i] - dot(totalsize,A[i],p); //calculation of residual
      mr[i] = r[i]/A[i][offset+i];   //calculation of modified residual
    }
    
    double [] sum = new double[1];
    double[] local_sum = {dot(rows_per_proc,mr,r)};
    comm.allReduce(local_sum,sum,1,MPI.DOUBLE,MPI.SUM);
    double c = sum[0];
  
    comm.allGather(mr,rows_per_proc,MPI.DOUBLE,p,rows_per_proc,MPI.DOUBLE);


    /* Part 4 */

    for(int k=0;k<totalsize;k++){
      for(int i=0;i<rows_per_proc;i++)
        z[i] = dot(totalsize,A[i],p);
      
      local_sum[0] = dot(rows_per_proc,z,Arrays.copyOfRange(p,offset,p.length));
      sum = new double[1];
      comm.allReduce(local_sum,sum,1,MPI.DOUBLE,MPI.SUM);

      double alpha = c/sum[0];

      for(int i=0;i<rows_per_proc;i++){
        x[i] = x[i] + alpha*p[offset+i];
        r[i] = r[i] - alpha*z[i];
      }

      /* Preconditioning Stage */
      for(int i=0;i<rows_per_proc;i++)
        mr[i] = r[i]/A[i][offset+i];
    
      local_sum[0] = dot(rows_per_proc,mr,r);
      comm.allReduce(local_sum,sum,1,MPI.DOUBLE,MPI.SUM);
    
      double d = sum[0]; //contains inner product of residual and modified residual

      local_sum[0] = dot(rows_per_proc,r,r);
      comm.allReduce(local_sum,sum,1,MPI.DOUBLE,MPI.SUM);

      //sum now contains inner product of residual and residual

      if(mynode == 0)
        System.out.println(k + "\t dot(mr,r) = " + d + "\t dot(r,r) = " + sum[0]);
 
      if(Math.abs(d) < tol) 
        break;
      if(Math.abs(sum[0]) < tol) 
        break;
    
      double beta = d/c;

      for(int i=0;i<rows_per_proc;i++)
        z[i] = mr[i] + beta*p[i+offset];

      comm.allGather(z,rows_per_proc,MPI.DOUBLE,p,rows_per_proc,MPI.DOUBLE);

      c = d; 
    }

    MPI.Finalize();
  }

  private static double dot(int N, double[] a, double[] b){
    double sum = 0d;
  
    for(int i=0;i<N;i++)
      sum += a[i]*b[i];

    return sum;
  }
}
