import mpi.*;

public class Chapter9c2P {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);

		Comm comm = MPI.COMM_WORLD;

		int rank = comm.getRank();
		int totalnodes = comm.getSize();

    final int numrows = 5;
    final int size = (int) Math.pow(2,log2(totalnodes+1)+1)-1;

    double[][] A = new double[numrows][size+1];

    if(rank==0){
        A[0][0] = -2.0; A[0][1] = 1.0;
        A[1][0] = 1.0; A[1][1] = -2.0; A[1][2] = 1.0;
        A[2][1] = 1.0; A[2][2] = -2.0; A[2][3] = 1.0;
    }else if(rank==(totalnodes-1)){
      int index = 2*rank;
      A[0][index-1] = 1.0; A[0][index] = -2.0; A[0][index+1] = 1.0;
      index = 2*rank+1;
      A[1][index-1] = 1.0; A[1][index] = -2.0; A[1][index+1] = 1.0;
      A[2][size-2] = 1.0; A[2][size-1] = -2.0;
    }else{
      for(int i=0;i<3;i++){
        int index = i + 2*rank;
        A[i][index-1] = 1.0;
        A[i][index]   = -2.0;
        A[i][index+1] = 1.0;
      }
    }

    for(int i=0;i<3;i++)
      A[i][size] = 2*rank+i;

    int numactivep = totalnodes;
    int[] activep = new int[totalnodes];
    for(int j=0;j<numactivep;j++)
      activep[j] = j;

    for(int j=0;j<size+1;j++){
      A[3][j] = A[0][j];
      A[4][j] = A[2][j];
    }

    /* Part 2 */
  
    for(int i=0;i<log2(size+1)-1;i++){
      for(int j=0;j<numactivep;j++){
        if(rank==activep[j]){
          int index1 = 2*rank + 1 - ((int) Math.pow(2,i));
          int index2 = 2*rank + 1 + ((int) Math.pow(2,i));
  
          double alpha = A[1][index1]/A[3][index1]; 
          double gamma = A[1][index2]/A[4][index2];
  
          for(int k=0;k<size+1;k++)
            A[1][k] -= (alpha*A[3][k] + gamma*A[4][k]);
  
          if(numactivep>1){
            if(j==0){
              comm.send(A[1],size+1,MPI.DOUBLE,activep[1],0);
            }else if(j==numactivep-1){
              comm.send(A[1],size+1,MPI.DOUBLE,activep[numactivep-2],1);
            }else if(j%2==0){
              comm.send(A[1],size+1,MPI.DOUBLE,activep[j-1],1);
              comm.send(A[1],size+1,MPI.DOUBLE,activep[j+1],0);
            }else{
              comm.recv(A[3],size+1,MPI.DOUBLE,activep[j-1],0);
              comm.recv(A[4],size+1,MPI.DOUBLE,activep[j+1],1);
            }
          }
        }
      }
   
      numactivep = 0;
      for(int j=activep[1];j<totalnodes;j=j+((int) Math.pow(2,i+1))){
          activep[numactivep++]=j;
      }
    }
  
  
    /* Part 3 */

    double[] x = new double[totalnodes];
    for(int j=0;j<totalnodes;j++)
      x[j] = 0.0;
  
    if(rank==activep[0]){
      x[rank] = A[1][size]/A[1][(size-1)/2];
    }

    //double[] tmp;
    for(int i=((int)log2(size+1))-3;i>=0;i--){
      double[] tmp = {x[rank]};
      comm.allGather(tmp,1,MPI.DOUBLE,x,1,MPI.DOUBLE);
      numactivep = 0;
      for(int j=activep[0]-((int)Math.pow(2,i));j<totalnodes;j=j+((int)Math.pow(2,i+1))){
        activep[numactivep++]=j;
      }

      for(int j=0;j<numactivep;j++){
        if(rank == activep[j]){
          x[rank] = A[1][size];
          for(int k=0;k<totalnodes;k++){
            if(k!=rank)
              x[rank] -= A[1][2*k+1]*x[k]; 
          }
          x[rank] = x[rank]/A[1][2*rank+1];
        }
      }
    }

    double[] tmp = {x[rank]};
    comm.allGather(tmp,1,MPI.DOUBLE,x,1,MPI.DOUBLE);

    /* Part 4 */
    for(int k=0;k<totalnodes;k++){
      A[0][size] -= A[0][2*k+1]*x[k]; 
      A[2][size] -= A[2][2*k+1]*x[k]; 
    }
    A[0][size] = A[0][size]/A[0][2*rank];
    A[1][size] = x[rank];
    A[2][size] = A[2][size]/A[2][2*rank+2];
  
		MPI.Finalize();
	}

  private static double log2(int x){
    return Math.log(x)/Math.log(2);
  }
}
